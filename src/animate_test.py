from dynamics import Dynamics
from simulate import Simulate
from animate import animate


def main():
    parameters = {
        'R': 0.1,
        'I': 1.,
        'J': 0.001,
        'm': 0.01,
        'g': 10.
    }
    d = Dynamics()

    rhs = d.get_rhs_num_fun(parameters)
    sim = Simulate(rhs)

    def feedback(t, state):
        theta,_,_,_ = state
        return 0.

    t,x,u  = sim.run(0, 5, [0.2, 1., 0., 0.], 1e-2, feedback)
    animate(t, x, 30, parameters)


if __name__ == '__main__':
    main()