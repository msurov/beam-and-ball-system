import matplotlib
import matplotlib.patches as patches
import matplotlib.pyplot as plt
from scipy.interpolate import make_interp_spline
import numpy as np
import matplotlib.animation as animation


font = {'size': 16}
matplotlib.rc('font', **font)
matplotlib.rcParams['pdf.fonttype'] = 42
matplotlib.rcParams['ps.fonttype'] = 42
matplotlib.rcParams['text.usetex'] = True


def plot_poly(verts, **kwargs):
    verts = np.array(verts)
    return plt.plot(verts[:,0], verts[:,1], **kwargs)


class Beam:
    def __init__(self, l):
        self.l = l
        self.theta = 0
        pts = self.get_pts()
        self.line, = plot_poly(pts, linewidth=2)
        self.pin = patches.CirclePolygon((0, 0), l/100, color='black', alpha=1., resolution=12)

    def get_pts(self):
        x = self.l * np.cos(self.theta)
        y = self.l * np.sin(self.theta)
        pts = np.array([
            (x, y),
            (-x, -y),
        ])
        return pts
    
    def update(self):
        pts = self.get_pts()
        self.line.set_data(pts[:,0], pts[:,1])
    
    def actors(self):
        return [self.line, self.pin]
    
    def patches(self):
        return [self.pin]


class Ball:
    def __init__(self, R):
        self.R = R
        self.theta = 0.
        self.s = 0.
        self.psi = 0.

        cx,cy = self.get_center()
        self.circle = patches.CirclePolygon((cx, cy), R, color='green', alpha=0.2, resolution=53)
        axes = self.get_axes()
        self.lines = [plot_poly(a, color='red', alpha=0.2)[0] for a in axes]

    def get_center(self):
        x = self.s * np.cos(self.theta) - self.R * np.sin(self.theta)
        y = self.s * np.sin(self.theta) + self.R * np.cos(self.theta)
        return np.array([x, y])
    
    def get_axes(self):
        x, y = self.get_center()
        R = self.R
        alpha = self.psi + self.theta
        a1 = np.array([
            [x + R * np.cos(alpha), y + R * np.sin(alpha)],
            [x - R * np.cos(alpha), y - R * np.sin(alpha)]
        ])
        a2 = np.array([
            [x + R * np.cos(alpha + np.pi/2), y + R * np.sin(alpha + np.pi/2)],
            [x - R * np.cos(alpha + np.pi/2), y - R * np.sin(alpha + np.pi/2)]
        ])
        a3 = np.array([
            [x + R * np.cos(alpha + np.pi/4), y + R * np.sin(alpha + np.pi/4)],
            [x - R * np.cos(alpha + np.pi/4), y - R * np.sin(alpha + np.pi/4)]
        ])
        a4 = np.array([
            [x + R * np.cos(alpha + 3*np.pi/4), y + R * np.sin(alpha + 3*np.pi/4)],
            [x - R * np.cos(alpha + 3*np.pi/4), y - R * np.sin(alpha + 3*np.pi/4)]
        ])
        return a1, a2, a3, a4

    def update(self):
        self.circle.xy = self.get_center()
        axes = self.get_axes()
        for a,line in zip(axes, self.lines):
            line.set_data(a[:,0], a[:,1])

    def actors(self):
        return self.lines + [self.circle]
    
    def patches(self):
        return [self.circle]


class BallAndBeam:

    def __init__(self, R, l):
        self.beam = Beam(l)
        self.ball = Ball(R)

    def move(self, theta, s, psi):
        self.beam.theta = theta
        self.ball.theta = theta
        self.ball.s = s
        self.ball.psi = psi

    def update(self):
        self.beam.update()
        self.ball.update()
    
    def patches(self):
        return self.beam.patches() + self.ball.patches()

    def actors(self):
        return self.beam.actors() + self.ball.actors()


def animate(t, state, fps, parameters):
    R = parameters['R']

    theta = state[:,0]
    s = state[:,1]
    fstate = make_interp_spline(t, state, k=1)

    l = np.max(s) + R

    xb = s * np.cos(theta) - R * np.sin(theta)
    yb = s * np.sin(theta) + R * np.cos(theta)
    xc = l * np.cos(theta)
    yc = l * np.sin(theta)

    xcmin = np.min(xc)
    xcmax = np.max(xc)
    ycmin = np.min(yc)
    ycmax = np.max(yc)

    xmin = min(np.min(xb) - R, np.min(-xc))
    xmax = max(np.max(xb) + R, np.max(xc))
    ymin = min(np.min(yb) - R, np.min(-yc))
    ymax = max(np.max(yb) + R, np.max(yc))

    plt.axis('equal')
    plt.gca().set_xlim([xmin, xmax])
    plt.gca().set_ylim([ymin, ymax])

    bb = BallAndBeam(R, l)
    for p in bb.patches(): plt.gca().add_patch(p)

    interval = 1/fps
    nframes = int((t[-1] - t[0]) / interval)

    def upt(frameidx):
        ti = frameidx * interval
        theta,s,_,_ = fstate(ti)
        psi = -s/R
        bb.move(theta, s, psi)
        bb.update()
        return bb.actors()

    plt.tight_layout()
    a = animation.FuncAnimation(plt.gcf(), upt, frames=nframes, interval=1000*interval, blit=False)
    plt.show()


def show(theta, s, psi, R, l):
    plt.axis('equal')
    plt.gca().set_xlim([-l-R, l+R])
    plt.gca().set_ylim([-l-R, l+R])
    bb = BallAndBeam(R, l)
    bb.move(theta, s, psi)
    bb.update()
    for p in bb.patches(): plt.gca().add_patch(p)
    plt.grid()
    plt.show()
