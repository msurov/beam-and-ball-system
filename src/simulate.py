from scipy.integrate import ode
import numpy as np


class Simulate:
    def __init__(self, rhs):
        self.u = None
        self.rhs = rhs

    def run(self, t1, t2, state1, step, feedback):

        nsteps = int((t2 - t1) / step + 0.5) + 1
        assert nsteps > 1, 'Invalid step specified'

        def rhs(t, state):
            return self.rhs(state, self.u)

        state = np.copy(state1)
        t = t1

        solver = ode(rhs)
        solver.set_integrator('dopri5', atol=1e-6, rtol=1e-6)
        solver.set_initial_value(state, t)
        self.u = feedback(t, state)

        tarr = np.zeros(nsteps)
        statearr = np.zeros((nsteps,) + state.shape)
        uarr = np.zeros((nsteps,) + np.shape(self.u))

        tarr[0] = t
        statearr[0] = state
        uarr[0] = self.u

        for i in range(1, nsteps):
            t = i * (t2 - t1) / (nsteps - 1) + t1
            solver.integrate(t)

            t = solver.t
            state = np.copy(solver.y)
            self.u = np.copy(feedback(t, state))

            tarr[i] = t
            statearr[i] = state
            uarr[i] = self.u

        return tarr, statearr, uarr

if __name__ == '__main__':
    from dynamics import Dynamics
    import matplotlib.pyplot as plt

    parameters = {
        'R': 0.1,
        'I': 1.,
        'J': 0.001,
        'm': 0.001,
        'g': 10.
    }
    d = Dynamics()

    rhs = d.get_rhs_num_fun(parameters)
    sim = Simulate(rhs)

    def feedback(t, state):
        theta,_,_,_ = state
        return -10*theta

    t,x,u  = sim.run(0, 5, [0.1, 1., 0., 0.], 1e-2, feedback)
    plt.plot(t, x[:,0])
    plt.plot(t, u)
    plt.show()
