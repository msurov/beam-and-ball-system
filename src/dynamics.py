from casadi import SX, vertcat, sin, cos, inv, substitute, Function


class Dynamics:

    def __init__(self):
        theta = SX.sym('theta')
        s = SX.sym('s')
        dtheta = SX.sym('dtheta')
        ds = SX.sym('ds')
        u = SX.sym('u')

        I = SX.sym('I')
        J = SX.sym('J')
        m = SX.sym('m')
        g = SX.sym('g')
        R = SX.sym('R')

        self.parameters = {
            'I': I,
            'J': J,
            'm': m,
            'g': g,
            'R': R
        }

        self.q = vertcat(theta, s)
        self.dq = vertcat(dtheta, ds)
        self.u = u

        M = SX.zeros((2, 2))
        M[0,0] = (J + I) / m + s**2 + R**2
        M[1,0] = \
        M[0,1] = -R - J / (m*R)
        M[1,1] = J / (m*R**2) + 1

        C = SX.zeros((2, 2))
        C[0,1] = 2*s*ds
        C[1,1] = -s*dtheta

        G = SX.zeros(2)
        G[0] = g * s * cos(theta) - g * R * sin(theta)
        G[1] = g * sin(theta)

        B = SX.zeros(2)
        B[0] = 1/m

        self.M = M
        self.C = C
        self.G = G
        self.B = B

        ddq = inv(M) @ (-C @ self.dq - G + B * self.u)

        self.rhs = vertcat(self.dq, ddq)


    def get_rhs_num_expr(self, parameters : dict):
        vars = list(self.parameters.values())
        vals = [parameters[k] for k in self.parameters.keys()]
        vars = vertcat(*vars)
        vals = vertcat(*vals)
        return substitute(self.rhs, vars, vals)


    def get_rhs_num_fun(self, parameters : dict):
        expr = self.get_rhs_num_expr(parameters)
        state = vertcat(self.q, self.dq)
        return Function('Dynamics', [state, self.u], [expr])


if __name__ == '__main__':
    d = Dynamics()
    rhs = d.get_rhs_num_fun({
        'R': 0.1,
        'I': 0.1,
        'J': 0.01,
        'm': 0.1,
        'g': 10.
    })
    print(rhs([0., 1., 0., 0.,], 0.))
